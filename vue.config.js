module.exports = {
    chainWebpack: config => {
        // https://stackoverflow.com/questions/40091008/how-to-compile-vuejs-single-file-component
        config.resolve.alias.set('vue$', 'vue/dist/vue.esm.js');
    }
};