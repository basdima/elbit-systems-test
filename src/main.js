import './initial-state';
import Vue from 'vue';
import './plugins/vuetify';
import './plugins/vuejs';
import './plugins/resize-sensor';
import App from './App.vue';
import router from './router';
import store from './store';
import 'roboto-fontface/css/roboto/roboto-fontface.css';
import '@fortawesome/fontawesome-free/css/all.css';
import './common/directives';

Vue.config.productionTip = false;



new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
