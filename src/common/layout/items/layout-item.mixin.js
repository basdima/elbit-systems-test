export default {
    props: {
        item: {
            type: Object,
            default() { return {} }
        },
        configurator: {
            type: Boolean,
            default: false
        }
    }
}