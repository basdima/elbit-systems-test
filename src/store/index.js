import Vue from 'vue'
import Vuex from 'vuex'
import AppStore from './app-store';
import DataStore from './data-store';
import VuexPersistence from 'vuex-persist';

Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
  modules: ['data'],
  storage: window.localStorage
});

export default new Vuex.Store({
  modules: {
    app: AppStore,
    data: DataStore
  },
  plugins: [
    vuexLocal.plugin
  ]
});


