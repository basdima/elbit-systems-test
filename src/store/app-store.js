export default {
    state: {
        title: 'ElbitSystems Test page',
        menu: false
    },
    mutations: {
        setCurrentPage(state, options) {
            const opt = {
                title: '',
                menu: false,
                ... options
            };
            state.title = opt.title;
            state.menu = opt.menu;
        },
    },
    actions: {
    }
};
