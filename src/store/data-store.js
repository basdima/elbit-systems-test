export default {
    state: {
        items: []
    },
    mutations: {
        removeItem(state, id) {
            state.items = state.items.filter((v) => v.id !== id);
        },
        addItem(state) {
            const nextId = state.items.reduce((id, item) => Math.max(id, item.id), 0);
            state.items = [ ... state.items , {
                id: nextId + 1
            }];
        },
        mutateData(state) {
            state.items = [ ... state.items ];
        }
    },
    actions: {
        storeData({ commit }) {
            commit('mutateData');
        }
    },
    getters: {
        getItem: (state) => (id) => {
            return state.items.find(i => i.id == id);
        }
    }
};
