export default {
    state: {
        dragElement: null
    },
    mutations: {
        setCurrentPage(state, options) {
            const opt = {
                title: '',
                menu: false,
                ... options
            };
            state.title = opt.title;
            state.menu = opt.menu;
        },
    },
    actions: {
    }
};
