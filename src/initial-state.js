const store = window.localStorage.getItem('vuex');
if (!store) {
    window.localStorage.setItem('vuex', JSON.stringify(
        {"data":{"items":[{"id":1,"layout":[{"x":0,"y":0,"w":12,"h":3,"i":3,"moved":false,"background":"/img/bg1.4349fd54.jpeg","component":"LayoutChartItem"},{"x":0,"y":3,"w":6,"h":6,"i":4,"moved":false,"background":"/img/bg3.d3b9be2a.jpg","":"","component":"LayoutButtonItem"},{"x":6,"y":3,"w":6,"h":6,"i":5,"moved":false,"background":"/img/bg1.4349fd54.jpeg","":"","component":"LayoutTextItem"},{"x":0,"y":9,"w":12,"h":7,"i":6,"moved":false,"background":"/img/bg1.4349fd54.jpeg","component":"LayoutRatingItem"}]},{"id":2,"layout":[{"x":0,"y":0,"w":12,"h":7,"i":1,"moved":false,"component":"LayoutChartItem"}]},{"id":3,"layout":[{"x":0,"y":0,"w":3,"h":2,"i":1,"moved":false,"component":"LayoutRatingItem"},{"x":0,"y":2,"w":3,"h":2,"i":2,"moved":false,"component":"LayoutButtonItem"},{"x":3,"y":0,"w":3,"h":4,"i":3,"moved":false,"component":"LayoutChartItem"}]}]}}
    ));
}
