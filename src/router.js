import Vue from 'vue';
import Router from 'vue-router';
import Layouts from './views/layouts';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'layouts',
      component: Layouts
    },
    {
      path: '/configurator/:id',
      name: 'configurator',
      component: () => import(/* webpackChunkName: "view-configurator" */ './views/configurator')
    }
  ]
});
