export default {
    props: ['preview'],
    methods: {
        dragStartHandler(event) {
            event.dataTransfer.setData('type', 'component');
            event.dataTransfer.setData('component', this.component.name);
        }
    }
}