import Vue from 'vue';
import ResizeSensor from 'vue-resize-sensor';

Vue.component('resize-sensor', ResizeSensor);
